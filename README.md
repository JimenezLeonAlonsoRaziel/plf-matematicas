# Conjuntos, aplicaciones y funciones (2002)

```plantuml
@startmindmap
+ Conjuntos, aplicaciones y funciones
++ transformación
+++ historia del cambio
++++ identificar cual elemento se convierte en el segundo
++++ convertir elementos
++++ toda función que mapea un conjunto X en otro conjunto
+++ transformación de transformación
++++ composición de aplicaciones
++++ se realiza aplicando las funciones
+++ transformación de conjuntos de números
++++ aplicaciones más fáciles de usar
+++++_ función
++++++ gráfica de funciones
++++++ constantes
++++++ funciones cuadráticas
++++++ funciones exponenciales
++++++ funciones algebraícas
++++++ funciones polinómicas
++++++ funciones logarítmicas
++ funciones
+++ sistema de coordenadas
++++ grafica de la función
+++++ serie de puntos
+++++ parábolas
+++++ línea recta
+++ aplicación de números
++++ fáciles de visualizar
++ conjuntos
+++_ definición
++++ relación de pertenencia
++++ colección de elementos
+++ conjunto referencial
++++ conjunto universal
+++++_ abarca
++++++ objetos simples
++++++ números
++++++ conjuntos de números
++++++ aquí ocurre toda la teoría
++++++ conjuntos de conjuntos de números
++++ conjunto vacío
+++++ conjunto lógico
+++++ necesidad lógica para cerrar las cosas
+++++ no contiene elementos
+++ cardinal de un conjunto
++++_ es
+++++ el número de elementos que conforman un conjunto
++++_ propiedades
+++++ fórmula para resolver la unión de conjuntos
+++++ acotación de cardinales
++++++ mayor que
++++++ menor que
++++++ igual que
+++ representaciones gráficas
++++ diagramas de Venn
+++++_ es
++++++ muestra colecciones de cosas por medio de líneas cerradas
+++++_ representados por
++++++ óvalos
++++++ círculos
++++++ elementos cerrados
+++++ no sirve para las demostraciones
++++ forma de mostrar
++++ lógica de clases
++++ tema de interés
++++ razonamiento diagrámatico
+++ operaciones con conjuntos
++++_ son
+++++ conjuntos armados sobre los conjuntos elementales de una forma más compleja
++++ conjuntos completos
++++ álgebra de conjuntos
++++_ básicas
+++++ unión
++++++_ elementos que pertenecen a alguno de ellos 
+++++ diferencia
++++++_ elementos del primer conjunto sin los elementos del segundo
+++++ intersección
++++++_ elementos que pertenecen simultáneamente a ambos
+++++ complementación
++++++_ elementos que no pertenecen a un conjunto dado
+++++ producto
++++++_ contiene todos los pares ordenados
+++ inclusión de conjuntos
++++_ es
+++++ la noción que hay entre dos conjuntos que es un trasunto de la relación de orden
++++ todos los elementos del primero pertenecen al segundo
+++++_ un conjunto está incluido al otro
++ aplicación
+++_ convierte a cada uno de los elementos del conjunto origen en un único elemento de un conjunto final
+++_ todos los elementos del primer conjunto deben de tener un transformado único del segundo conjunto
+++_ debe ser única
+++_ debe tener una aplicación
+++_ tipos
++++ imagen inversa
+++++_ aplicación que a cada subconjunto del conjunto final le corresponde el conjunto de elementos del conjunto inicial
++++ imagen directa
+++++_ hace referencia al valor que le corresponde bajo la función
++++ aplicación inyectiva
+++++_ si dos elementos tienen la misma imagen, se dice que son el mismo elemento
++++ aplicación subyectiva
+++++_ si es equivalente
++++ aplicación biyectiva
+++++_ si es inyectiva y subyectiva
++ imagen
+++_ inversa, antiimagen, contraimagen
++++ aplicación que a un conjunto le hace corresponder otro
++++_ aplicaciones
+++++ topología
+++++ teoría de la medida
++ pensamiento matemático
+++_ vocaciones de las matemáticas
+++_ dominio de las ideas
+++_ razonar
+++_ filosofía de las matemáticas
++ propiedades de números cardinales
+++_ relaciona dos conjuntos solo si todos tienen el mismo número de elementos
+++_ serie de relaciones
+++_ obtener mayores, menores y niveles
+++_ acotación de cardinales
+++_ fórmula de cardinales
@endmindmap
```


# Funciones (2010)

```plantuml
@startmindmap
+ Funciones
++_ definiciones
+++ regla de correspondiente entre dos conjuntos de tal manera que a cada elemento de primer conjunto le corresponde uno y solo un elemento del segundo conjunto
++++_ principalmente
+++++ conjunto de número reales
++ usos
+++ se calculan para cada función
++_ son
+++ ideas básicas de las matemáticas que pueden llegar a temas profundos
++_ refleja
+++ el pensamiento humano
+++_ reglas
++++ comprender el entorno
++++ resolver problemas
++_ describen
+++ fenómenos
++++ científicos
+++++_ ejemplo
++++++ control de elementos químicos
++++ psicológicos
+++++_ ejemplo
++++++ comportamiento de una persona en un tiempo determinado
++++ temperatura
+++++_ ejemplo
++++++ cambios climáticos
++++ economía
+++++_ ejemplo
++++++ variaciones de las monedas
++++ cotidianos
+++++_ ejemplo
++++++ venta de productos
+++ para cada valor de X le corresponde un valor de Y
++ tipos
+++ crecientes
++++ aumenta la variable independiente
+++ decrecientes
++++ variables que disminuyen 
+++ sencillas
++++ operaciones para obtener sus valores
+++ complejas
++++ mezcla de funciones sencillas
+++++_ ejemplos
++++++ derivadas
++ comportamiento
+++ máximos
++++_ la función alcanza el mayor valor
+++ mínimos
++++_ la función alcanza el mínimo valor
+++ límite
++++_ hace referencia a las divisiones existentes
+++ intervalo
++++_ conjunto de los valores que toma una magnitud entre dos límite dados
+++ dominio
++++_ conjunto de todos los valores de entrada que al aplicar la función llevan un valor de salida
+++ continuidad
++++_ es una línea seguida, no interrumpida, en la gráfica
+++ descontinuidad
++++_ puntos donde el denominador es 0, y es contínua en otra parte
+++ derivada
++++ reglas
+++++ regla de la cadena
+++++ derivada de suma
+++++ derivada de una constante
+++++ derivada de un producto
+++++ derivada de un cociente
+++++ derivada de las funciones trigonométricas
+++++ derivada de una constante por una función
+++++ derivada de una potencia entera positiva
++++ aproximaciones
+++++_ resolver el problema de la aproximación de una función compleja por medio de una función línea
++ representaciones gráficas
+++_ sirven para representar en un plano
++++ gráficas
++++ imagen
++++ cartesiana
+++++_ inventada por René Descartes
+++++ conjunto de números reales
++++++ eje
+++++++_ X
+++++++_ Y
+++++++_ se obtienen imágenes
++ aplicaciones
+++ transformaciones
++++ de un conjunto
++++ otro conjunto
@endmindmap
```

# La matemática del computador (2002)

```plantuml
@startmindmap
+ La matemática del computador
++_ utiliza
+++ aritmética binaria por el ALU
++++_ sistema base 2
+++++ adición
+++++ sustracción
+++++ multiplicación
+++++ división
++ sistema binario
+++ sistema simple
++++_ basado en
+++++ sistema booleano
+++++ representación con 0 y 1
+++_ representa
++++ imagenes
++++ archivos
++++ videos
+++ aplicaciones
++++_ interruptores
+++++ 0 sin corriente
+++++ 1 con corriente
++++_ circuitos
++++_ corrientes
++ computador
+++_ es
++++ máquina que soluciona problemas
+++_ utiliza
++++ sistema binario
++++ paso de corriente
+++++ posiciones de memoria
+++++ punto flotante
+++++ número entero
++ aritmética del computador
+++_ es
++++ cálculo con números como la aproximación de un número
+++++_ ejemplo
++++++ PI
++++++ número de punto flotante
++++++ raíz de dos
++ técnicas
+++ técnica de redondeo
++++ no considera los números decimales
++++_ ejemplo
+++++ 3.1415 se convierte en 3
+++ técnica de truncamiento
++++ corta el número exacto sin preocupar dónde continua
+++++_ ejemplo
++++++ 9.186325 se convierte en 9.18
++ matemáticas
+++_ es
++++ la ciencia que parte de una deducción lógica
+++_ permite
++++ estudiar los valores abstractos como los números
+++_ ayudan a resolver problemas cotidianos
+++ son importantes para el desarrollo de los computadores
+++_ números
++++ reales
++++ decimales
++++ fraccionales
++++ abstractos o teóricos
+++ representación interna
++++ números enteros
+++++_ representan
++++++ magnitud de signo
++++++ complemento a dos
++++++ exceso
++++_ indica como transformar un paso de corriente de 0 y 1 en una serie de posiciones de memoria
+++ aritmética finita
++++ truncamiento
+++++_ es cortar unas cifras a al derecha a partir de una dada
+++++_ cortar un número que tiene infinitos decimales
++++ redondeo 
+++++ evita errores con números
+++++ es un truncamiento refinado
+++++ evitar tener los menos errores posibles
++++ digitos significativos
+++++ miden la presición general relativa de un valor
++ sistemas de numeración
+++ sistema decimal
++++_ base aritmética en el número 10
+++ sistema hexadecimal
++++_ es posicional de base 16
+++ sistema octal
++++_ usado en la informática que no requiere utilizar otros símbolos deferentes de los dígitos
++++_ base 8
+++ aritmética de punto flotante
++++_ los cálculos se realizan con una presición limitada
+++ desbordamiento
++++_ es el fallo informático que se da cuando el código almacenado en un registro supera su valor máximo
++ cálculos
+++ aritmética de precisión finita
++++_ es la aproximación de un número
++++ infinitas cifras
+++++_ ejemplos
++++++ números reales
++++++ números decimales
++++++ números fraccionales
++++ cifras significativas
+++++ representan el uso de una o más escalas de incertidumbre en determinadas aproximaciones
++++ técnicas
+++++_ redondeo
+++++_ truncamiento
+++++_ mantisa
++++++ encontrado en los logaritmos
++++++_ es la diferencia entre un número y su parte entera
+++ deducción lógica
++++_ contiene valores abstractos
++++ es la resolución de problemas cotidianos
++ representación de los números
+++_ es la cmbinación de signos para
++++ identificación de cantidades correspondientes
+++++_ como
++++++ números enteros
++++++ mantisa
@endmindmap
```
